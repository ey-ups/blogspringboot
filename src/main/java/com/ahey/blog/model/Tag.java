package com.ahey.blog.model;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "tags")
public class Tag {

    @Id
    private Long id;

    @Enumerated(EnumType.STRING)
    private ETag tag;

    @ManyToMany(mappedBy = "tags",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Article> articles;
}
