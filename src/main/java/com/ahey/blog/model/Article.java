package com.ahey.blog.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "articles")
public class Article {

    @Id
    private Long id;

    private String header;
    private String content;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @OneToMany(mappedBy = "article" ,fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Comment> comment;


    @ManyToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Tag> tags;


}
